import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types'
import { useState } from 'react';

export default function CourseCard({ course }) {

    // You can also double destructure
    // CourseCard ({course}) {
    //     const {name, description, price} = course;
    // }

    // Destructuring the contents of 'course'
    const { name, description, price } = course;

    // A state is just like a variable but with the concept of getters and setters. The getter is responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook is responsible for setting the initial value of the state.
    const [count, setCount] = useState(0);

    const [seats, setSeats] = useState(30);

    function enroll() {
        // The setCount function can use the previous value of the state and add/modify to it. 
        setCount(prev_value => prev_value + 1);
    }

    function reserveSeat() {
        setSeats(prev_value => prev_value - 1);
    }

    function enrollAndSeat() {
        if (seats === 0) {
            return alert("No more seats available");
        }
        enroll();
        reserveSeat();
    }


    // Another solution for activity S55
    /* 
    const [seats, setSeats] = useState(30);
    function enroll() {
        if (seats > 0) {
            setCount(prev_value => prev_value + 1);
            setSeats(prev_value => prev_value - 1)
        } else {
            alert("No more seats available")
        }
    } */
    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

                <Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{count}</Card.Text>

                <Card.Subtitle>Seats:</Card.Subtitle>
                <Card.Text>{seats}</Card.Text>

                <Button variant='primary' onClick={enrollAndSeat}>Enroll</Button>
            </Card.Body>
        </Card>

    );
}

 
// PropTypes is used for validating the data from the props
CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
