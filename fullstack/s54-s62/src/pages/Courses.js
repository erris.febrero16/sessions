import coursesData from '../data/coursesData.js';
import CourseCard from '../components/CourseCard.js';

export default function Courses(){
	return(
		<>
			<h1>Courses</h1>
			<CourseCard course={coursesData[0]}/>
			<CourseCard course={coursesData[1]}/>
		</>
	)
}