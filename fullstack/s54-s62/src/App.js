import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import {Container} from 'react-bootstrap';

function App() {
  return (
    <>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
        <Register/>
      </Container>
    </>
  );
}

export default App;
