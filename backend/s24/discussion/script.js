// // [SECTION] While Loop
// let count = 5;

// while (count !== 0){
// 	console.log("Current value of count: " + count);
// 	count--;
// }


// // [SECTION] Do While Loop
// let number = Number(prompt("Give me a number: "));

// do {
// 	console.log("Current value of number: " + number);

// 	number += 1;
// } while(number < 10)

// // [SECTION] For Loop
// for (let count = 0; count <= 20; count++){
// 	console.log("Current for loop value: " + count);
// }

let my_string = "erris febrero";

// // To get the length of a string
// console.log(my_string.length);

// // To get a specific letter in a string
// console.log(my_string[2]);

for (let index = 0; index < my_string.length; index++){
	if (my_string[index] == 'e' || my_string[index] == 'i' || my_string[index] == 'o'){
		continue ;
	} else { 
		console.log(my_string[index]);
	}
} 