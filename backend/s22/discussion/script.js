// Arguments and parameters
function printName(name){
	console.log("I'm the real " + name);
}

printName("Slim Shady");

function checkDivisibilityBy2(number){
	let result = number % 2;

	console.log("The remainder of " + number + " divided by 2 is " + result);
}

checkDivisibilityBy2(10);

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");

let user_name = prompt("Enter your username:");

function displayWelcomeMessageForUser(userName){
	alert("Welcome back to Valorant " + userName);
}

displayWelcomeMessageForUser(user_name);